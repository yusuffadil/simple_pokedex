import React from 'react';
import './App.css';
import { Layout } from 'antd';

import Header from './Components/Header';
import Content from './Page/index'

const { Footer } = Layout;

function App() {
  return (
    <Layout>
        <Header />
        <Content />
        <Footer style={{ textAlign: 'center' }}>Simple Pokedex ©2020 Created by bye</Footer>
    </Layout>
  );
}

export default App;
