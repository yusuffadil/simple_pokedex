import React from 'react';
import { Button } from 'antd';
import { StepBackwardOutlined, StepForwardOutlined } from '@ant-design/icons';

export default function Pagination({ gotoNextPage, gotoPrevPage }) {
    return (
        <div style={{ display: 'inline-flex', justifyContent: 'center', alignItems: 'center'}}>
            {gotoPrevPage && 
                <Button type="primary" onClick={gotoPrevPage}><StepBackwardOutlined /> Previous</Button>
            }
            {gotoNextPage && 
                <Button type="primary" onClick={gotoNextPage}>Next <StepForwardOutlined /></Button>}
        </div>
    )
}
