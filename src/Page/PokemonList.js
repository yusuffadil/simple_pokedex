import React from 'react';
import { List, Card } from 'antd';

const { Meta } = Card;

export default function PokemonList({ pokemon, url }) {
    return (
        <div className="site-card-wrapper">
            <List
                grid={{ gutter: 16, column: 4 }}
                dataSource='1'
                renderItem={item => (
                    pokemon.map((p, i) => (
                        <Card
                            hoverable
                            cover={<img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${url[i]}.svg`} alt={p} height={100} mode='fit' />}
                        >
                            <Meta title={p} />
                        </Card>
                    ))
                )}
            />
        </div>
    )
}